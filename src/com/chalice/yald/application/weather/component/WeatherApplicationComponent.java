package com.chalice.yald.application.weather.component;

import com.chalice.yald.application.weather.dialog.WeatherDialog;

/**
 * 应用启动监听
 * @author laozhou
 */
public class WeatherApplicationComponent {

    /**
     * IDEA启动时加载 该应用实例级别组件 only once and one
     */
    public void initComponent() {
        WeatherDialog dialog = new WeatherDialog();
        dialog.show();
    }
    public WeatherApplicationComponent()
    {
        initComponent();
    }
}