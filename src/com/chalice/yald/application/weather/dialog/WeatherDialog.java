package com.chalice.yald.application.weather.dialog;

import com.chalice.yald.application.weather.bean.WeatherBO;
import com.chalice.yald.util.BusiUtil;
import com.intellij.openapi.ui.DialogWrapper;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * 窗口，叉掉窗口后进入IDEA
 *
 * @author laozhou
 */
public class WeatherDialog extends DialogWrapper {
    /**
     * 弹窗内容包裹对象
     */
    private JLabel label;

    /**
     * 启动IDEA时，创建一个会话面板（弹窗），标题是【每天一碗毒鸡汤】
     */
    public WeatherDialog() {
        super(true);
        setTitle("江西省南昌市青山湖区");
        init();
    }

    /**
     * 获取并展示面板中部显示的内容
     *
     * @return
     */
    @Nullable
    @Override
    protected JComponent createCenterPanel() {
        JPanel panel = new JPanel();

        WeatherBO weather = BusiUtil.getWeatherFromGaode();
        StringBuilder sb = new StringBuilder();
        if (null == weather) {
            sb.append("假如天气从地球消失了，不要放弃，直面过去与未来！");
        } else {
            sb.append("天气").append(weather.getWeather())
                    .append("，当前气温 ").append(weather.getTemperature()).append(" °C，")
                    .append(weather.getWinddirection()).append("风，湿度").append(weather.getHumidity())
                    .append("，风力级别").append(weather.getWindpower());
        }
        label = new JLabel(sb.toString());
        panel.add(label);
        return panel;
    }

    /**
     * 覆盖并创建底部的面板
     *
     * @return
     */
    @Override
    protected JComponent createSouthPanel() {
        BusiUtil.getTauntContent();
        JPanel panel = new JPanel();
        JButton button = new JButton("干碗鸡汤提提神");
        panel.add(button);
        button.addActionListener(e -> label.setText(BusiUtil.getTauntContent()));
        return panel;
    }
}