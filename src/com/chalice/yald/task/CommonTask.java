package com.chalice.yald.task;

import com.chalice.yald.util.BusiUtil;

/**
 * @author laozhou
 * @date 2024/1/26
 */
public class CommonTask implements Runnable{
    @Override
    public void run() {
        BusiUtil.getTauntContent();
    }
}