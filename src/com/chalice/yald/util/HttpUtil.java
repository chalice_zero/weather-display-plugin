package com.chalice.yald.util;

import cn.hutool.core.util.ObjectUtil;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author laozhou
 */
public class HttpUtil {
    private HttpUtil() {
    }

    /**
     * 向指定URL发送GET方法的请求
     *
     * @param url      发送请求的URL
     * @param paramMap 请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return URL 所代表远程资源的响应结果
     */
    public static String sendGetByParam(String url, Map<String, Object> paramMap) {
        String result = "";
        BufferedReader in = null;
        try {
            String urlNameString = url;
            if (ObjectUtil.isNotEmpty(paramMap)) {
                String param = "?";
                for (Map.Entry<String, Object> entry : paramMap.entrySet()) {
                    String k = entry.getKey();
                    Object v = entry.getValue();
                    param += (k + "=" + v + "&");
                }
                param = param.substring(0, param.lastIndexOf('&'));
                urlNameString = url + param;
            }
            URL realUrl = new URL(urlNameString);
            // 打开和URL之间的连接
            URLConnection connection = realUrl.openConnection();
            // 设置通用的请求属性
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // 建立实际的连接
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
            // 遍历所有的响应头字段
            for (String key : map.keySet()) {
                System.out.println(key + "--->" + map.get(key));
            }
            // 定义 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送GET请求出现异常！" + e);
            e.printStackTrace();
        }
        // 使用finally块来关闭输入流
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 向指定URL发送GET请求
     *
     * @param url 请求地址
     * @return
     */
    public static String sendGet(String url) {
        InputStream is = null;
        ByteArrayOutputStream os = null;
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/5.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // 建立实际的连接
            conn.connect();
            is = conn.getInputStream();
            os = new ByteArrayOutputStream();
            byte[] bytes = new byte[1024];
            int i = -1;
            while ((i = is.read(bytes)) != -1) {
                os.write(bytes, 0, i);
            }
            return new String(os.toByteArray(), "UTF-8");
        } catch (Exception e) {
            System.out.println("发送GET请求出现异常！" + e);
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (is != null) {
                    is.close();
                    is = null;
                }
                if (os != null) {
                    os.close();
                    os = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 向指定URL发送GET请求
     *
     * @param url 请求地址
     * @return
     */
    public static String sendGet(String url, Map<String, String> headMap) {
        InputStream is = null;
        ByteArrayOutputStream os = null;
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/5.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            headMap.forEach((k, v) -> {
                conn.setRequestProperty(k, v);
            });
            // 建立实际的连接
            conn.connect();
            is = conn.getInputStream();
            os = new ByteArrayOutputStream();
            byte[] bytes = new byte[1024];
            int i = -1;
            while ((i = is.read(bytes)) != -1) {
                os.write(bytes, 0, i);
            }
            return new String(os.toByteArray(), "UTF-8");
        } catch (Exception e) {
            System.out.println("发送GET请求出现异常！" + e);
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (is != null) {
                    is.close();
                    is = null;
                }
                if (os != null) {
                    os.close();
                    os = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 向指定URL发送POST请求
     *
     * @param url   请求地址
     * @param param 请求参数
     * @return
     */
    public static String sendPost(String url, String param) {
        PrintWriter out = null;
        InputStream is = null;
        ByteArrayOutputStream os = null;
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/5.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setConnectTimeout(1000 * 3); // 设置超时时间
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(param);
            // flush输出流的缓冲
            out.flush();
            is = conn.getInputStream();
            os = new ByteArrayOutputStream();
            byte[] bytes = new byte[1024];
            int i = -1;
            while ((i = is.read(bytes)) != -1) {
                os.write(bytes, 0, i);
            }
            return new String(os.toByteArray(), "UTF-8");
        } catch (Exception e) {
            System.out.println("发送POST请求出现异常！" + e);
            e.printStackTrace();
            return "发送POST请求出现异常！";
        } finally {
            try {
                if (is != null) {
                    is.close();
                    is = null;
                }
                if (os != null) {
                    os.close();
                    os = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 向指定URL发送POST请求
     *
     * @param url   请求地址
     * @param param 请求参数
     * @return
     */
    public static String sendPost(String url, String param, Map<String, String> headMap) {
        PrintWriter out = null;
        InputStream is = null;
        ByteArrayOutputStream os = null;
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/5.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            conn.setRequestProperty("Content-Type", "application/json");
            headMap.forEach((k, v) -> {
                conn.setRequestProperty(k, v);
            });
            conn.setConnectTimeout(1000 * 3); // 设置超时时间
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(param);
            // flush输出流的缓冲
            out.flush();
            is = conn.getInputStream();
            os = new ByteArrayOutputStream();
            byte[] bytes = new byte[1024];
            int i = -1;
            while ((i = is.read(bytes)) != -1) {
                os.write(bytes, 0, i);
            }
            return new String(os.toByteArray(), "UTF-8");
        } catch (Exception e) {
            System.out.println("发送POST请求出现异常！" + e);
            e.printStackTrace();
            return "发送POST请求出现异常！";
        } finally {
            try {
                if (is != null) {
                    is.close();
                    is = null;
                }
                if (os != null) {
                    os.close();
                    os = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * POST提交表单
     *
     * @param serverUrl
     * @param param
     * @param timeout
     * @return
     * @throws Exception
     */
    public static String doPost(String serverUrl, Map<String, Object> param, int timeout) throws Exception {
        if (ObjectUtil.isEmpty(serverUrl)) {
            return "";
        }
        BufferedReader reader = null;
        OutputStreamWriter wr = null;

        try {
            URL url = new URL(serverUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            // 设置允许输出
            conn.setDoOutput(true);

            conn.setRequestMethod("POST");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("contentType", "application/x-www-form-urlencoded");

            StringBuilder paramSb = new StringBuilder();
            for (String key : param.keySet()) {
                paramSb.append(key).append("=").append(param.get(key)).append("&");
            }
            String paramStr = paramSb.toString();
            if (!ObjectUtil.isEmpty(paramStr)) {
                paramStr = paramStr.substring(0, paramStr.lastIndexOf("&"));
            }

            if (timeout == 0) {
                conn.setConnectTimeout(1000 * 3);
            }

            // 设置不用缓存
            conn.setUseCaches(false);
            // 开始连接请求
            conn.connect();
            wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            // 写入请求的字符串
            wr.write(paramStr);
            wr.flush();

            reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            StringBuilder responseBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                responseBuilder.append(line).append("\n");
            }

            // 请求返回的状态
            if (conn.getResponseCode() == 200) {
                conn.disconnect();
                return responseBuilder.toString();
            } else {
                return conn.getResponseCode() + "";
            }
        } catch (Exception e) {
            throw e;
        } finally {
            reader.close();
            wr.close();
        }
    }

    /**
     * POST提交表单
     *
     * @param serverUrl
     * @param param
     * @param headMap
     * @return
     * @throws Exception
     */
    public static String doPost(String serverUrl, Map<String, Object> param, Map<String, String> headMap) throws Exception {
        if (ObjectUtil.isEmpty(serverUrl)) {
            return "";
        }
        BufferedReader reader = null;
        OutputStreamWriter wr = null;

        try {
            URL url = new URL(serverUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            // 设置允许输出
            conn.setDoOutput(true);

            conn.setRequestMethod("POST");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("contentType", "application/x-www-form-urlencoded");
            headMap.forEach((k, v) -> {
                conn.setRequestProperty(k, v);
            });
            StringBuilder paramSb = new StringBuilder();
            for (String key : param.keySet()) {
                paramSb.append(key).append("=").append(param.get(key)).append("&");
            }
            String paramStr = paramSb.toString();
            if (!ObjectUtil.isEmpty(paramStr)) {
                paramStr = paramStr.substring(0, paramStr.lastIndexOf("&"));
            }

            // 设置不用缓存
            conn.setUseCaches(false);
            // 开始连接请求
            conn.connect();
            wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            // 写入请求的字符串
            wr.write(paramStr);
            wr.flush();

            reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            StringBuilder responseBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                responseBuilder.append(line).append("\n");
            }

            // 请求返回的状态
            if (conn.getResponseCode() == 200) {
                conn.disconnect();
                return responseBuilder.toString();
            } else {
                return conn.getResponseCode() + "";
            }
        } catch (Exception e) {
            throw e;
        } finally {
            reader.close();
            wr.close();
        }
    }

    /**
     * asUrlParams方法慨述: 将map转化key=123&v=456格式 为只要确保你的编码输入是正确的,就可以忽略掉
     */
    public static String asUrlParams(Map<String, String> source) {
        Iterator<String> it = source.keySet().iterator();
        StringBuilder paramStr = new StringBuilder();
        while (it.hasNext()) {
            String key = it.next();
            String value = source.get(key);
            if (ObjectUtil.isEmpty(value)) {
                continue;
            }
            try {
                // URL 编码
                value = URLEncoder.encode(value, "utf-8");
            } catch (UnsupportedEncodingException e) {
                // do nothing
            }
            paramStr.append("&").append(key).append("=").append(value);
        }
        // 去掉第一个&
        return paramStr.substring(1);
    }

    //    public static void main(String[] args)
    //    {
    //        String access_token = sendGet("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxbed761d8a87a1cba&secret=37faf88302f54f569dedea7305937595");
    //        System.err.println("access_token:" + access_token);
    //
    //        String news = sendPost("https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=",
    //                "{\"type\":\"news\",\"offset\":0,\"count\":1}");
    //        System.err.println("news:" + news);
    //    }
}