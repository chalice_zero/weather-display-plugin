package com.chalice.yald.util;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.chalice.yald.application.weather.bean.WeatherBO;

/**
 * @author laozhou
 * @date 2024/1/25
 */
public class BusiUtil {
    public static final String STATUS_ONE = "1";

    public static String getTauntContent() {
        try {
            String resp = HttpUtil.sendGet("https://api.nextrt.com/V1/Dutang");
            if (ObjectUtil.isNotEmpty(resp)) {
                JSONObject jsonObj = JSONUtil.parseObj(resp);
                String status = jsonObj.getStr("status");
                if (STATUS_ONE.equals(status)) {
                    JSONArray data = jsonObj.getJSONArray("data");
                    JSONObject taunt = data.getJSONObject(0);
                    return taunt.getStr("content");
                }
            }
        } catch (Exception e) {
        }
        return getFinalTauntContent();
    }

    /**
     * 毒鸡汤内容
     *
     * @return
     * @deprecated
     */
    public static String getFinalTauntContent()
    {
        return "又一天过去了，怎么样，是不是梦想更遥远了？";
    }

    /**
     * 获取高德天气
     * {
     *     "status": "1",
     *     "count": "1",
     *     "info": "OK",
     *     "infocode": "10000",
     *     "lives": [
     *         {
     *             "province": "江西",
     *             "city": "青山湖区",
     *             "adcode": "360111",
     *             "weather": "晴",
     *             "temperature": "4",
     *             "winddirection": "东南",
     *             "windpower": "≤3",
     *             "humidity": "61",
     *             "reporttime": "2024-01-26 21:37:23",
     *             "temperature_float": "4.0",
     *             "humidity_float": "61.0"
     *         }
     *     ]
     * }
     */
    public static WeatherBO getWeatherFromGaode()
    {
        String daodeKey = "高德开放平台key";
        daodeKey = "1100518eee51752a420100a0a1276a881";
        String url = "https://restapi.amap.com/v3/weather/weatherInfo?" +
                "key=" + daodeKey + "&city=360111&extensions=base&output=json";
        try {
            String resp = HttpUtil.sendGet(url);
            JSONObject jsonObj = JSONUtil.parseObj(resp);
            String status = jsonObj.getStr("status");
            if (STATUS_ONE.equals(status))
            {
                JSONArray lives = jsonObj.getJSONArray("lives");
                JSONObject weatherJson = lives.getJSONObject(0);
                WeatherBO weatherBO = weatherJson.toBean(WeatherBO.class);
                return weatherBO;
            }
        } catch (Exception e)
        {
        }
        return null;
    }
}